package cmuguide.demo.controller

import cmuguide.demo.entity.User
import cmuguide.demo.entity.dto.UserDto
import cmuguide.demo.service.UserService
import cmuguide.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class UserController {

    @Autowired
    lateinit var userService: UserService

    @GetMapping("/user")
    fun getAllUser(): ResponseEntity<Any> {
        return ResponseEntity.ok(userService.getUsers());
    }

    @PostMapping("user/addUser")
    fun addUser(
            @RequestBody user: User): ResponseEntity<Any> {
        val output = userService.save(user)
        val outputDto = output
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
    }

//    @PostMapping("/comment/editCommentId/{commentId}")
//    fun editCommentByCommentId(@PathVariable("commentId") commentId: Long,
//                               @RequestBody description: String): ResponseEntity<Any> {
//        val comment = commentService.editCommentByCommentId(commentId, description)
//        val outputProduct = comment
//        outputProduct?.let { return ResponseEntity.ok(it) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("The comment is not found")
//    }




}