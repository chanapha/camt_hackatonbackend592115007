package cmuguide.demo.controller

import cmuguide.demo.entity.Comment
import cmuguide.demo.entity.dto.CommentDto
import cmuguide.demo.service.CommentService
import cmuguide.demo.service.ShopListService
import cmuguide.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class CommentController {

    @Autowired
    lateinit var commentService: CommentService

    @Autowired
    lateinit var shopListService: ShopListService

    @GetMapping("/comment")
    fun getAllComment(): ResponseEntity<Any> {
        return ResponseEntity.ok(commentService.getComments())
    }

    @GetMapping("/comment/shopListID/{shopListId}")
    fun getCommentsByShopListId(
            @PathVariable shopListId: Long): ResponseEntity<Any> {
        val output = shopListService.getCommentsByShopListId(shopListId)
        return ResponseEntity.ok(output)
    }

    @PostMapping("/comment/addCommentTo/{shopListId}")
    fun addCommentToShoplist(
            @PathVariable shopListId: Long,
            @RequestBody comment: CommentDto
    ) : ResponseEntity<Any>{
        val output = commentService.addCommentToShoplist(shopListId, comment)
        val outputDto = output
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
    }

    @DeleteMapping("/comment/id/{commentId}")
    fun deleteComment(@PathVariable("commentId") commentId: Long): ResponseEntity<Any> {
        val comment = commentService.remove(commentId)
        val outputProduct = comment
        outputProduct?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("The comment is not found")
    }

    @PostMapping("/comment/editCommentId/{commentId}")
    fun editCommentByCommentId(@PathVariable("commentId") commentId: Long,
                               @RequestBody description: String): ResponseEntity<Any> {
        val comment = commentService.editCommentByCommentId(commentId, description)
        val outputProduct = comment
        outputProduct?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("The comment is not found")
    }


}