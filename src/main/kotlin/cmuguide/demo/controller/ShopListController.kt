package cmuguide.demo.controller

import cmuguide.demo.service.CommentService
import cmuguide.demo.service.ShopListService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ShopListController {

    @Autowired
    lateinit var shopListService: ShopListService

    @GetMapping("/shopList")
    fun getAllShopList(): ResponseEntity<Any> {
        return ResponseEntity.ok(shopListService.getShopLists())
    }




}