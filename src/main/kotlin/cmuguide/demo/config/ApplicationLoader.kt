package cmuguide.demo.config

import cmuguide.demo.entity.Comment
import cmuguide.demo.entity.ShopList
import cmuguide.demo.entity.User
import cmuguide.demo.repository.CommentRepository
import cmuguide.demo.repository.ShopListRepository
import cmuguide.demo.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
class ApplicationLoader: ApplicationRunner {
    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var commentRepository: CommentRepository

    @Autowired
    lateinit var shopListRepository: ShopListRepository


    @Transactional
    override fun run(args: ApplicationArguments?) {

        var user1 = userRepository.save(User("JaneyJane", "Chanapha","Janey@gmail.com","1111"))
        var user2 = userRepository.save(User("Benjamin", "Jamin","Ben@gmail.com","2222"))
        var user3 = userRepository.save(User("Jametaylor", "Collin","Jame@gmail.com","3333"))

        var comment1 = commentRepository.save(Comment("This shop is very good", false))
        var comment2 = commentRepository.save(Comment("This shop is very bad", false))
        var comment3 = commentRepository.save(Comment("This shop is awful", false))

        var shopList1 = shopListRepository.save(ShopList("GYPSO","Gypso offers Bohemian style accessories.","ร้านค้า"))
        var shopList2 = shopListRepository.save(ShopList(" PRA TAILORS","Offers ready-made and custom-made suit uniforms, dresses, shirts by skilled tailors to make you look the best on your special occasions.","ร้านค้า"))
        var shopList3 = shopListRepository.save(ShopList(" Maneerat","Monk supplies store, located on Phra Sumen Road, offering all kinds of religious ritual supplies at reasonable prices.","ร้านค้า"))


        shopList1.comments.add(comment1)

        shopList2.comments.add(comment2)

        shopList3.comments.add(comment3)

        comment1.user = user1

        comment2.user = user1

        comment3.user = user2

        comment3.user = user3

    }

}