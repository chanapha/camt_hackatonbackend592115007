package cmuguide.demo.repository

import cmuguide.demo.entity.User
import org.springframework.data.repository.CrudRepository

interface UserRepository: CrudRepository<User, Long>{

}