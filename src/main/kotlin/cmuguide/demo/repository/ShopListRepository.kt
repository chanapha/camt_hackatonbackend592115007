package cmuguide.demo.repository

import cmuguide.demo.entity.ShopList
import org.springframework.data.repository.CrudRepository

interface ShopListRepository: CrudRepository<ShopList, Long>