package cmuguide.demo.repository

import cmuguide.demo.entity.Comment
import org.springframework.data.repository.CrudRepository

interface CommentRepository: CrudRepository<Comment, Long>{
    fun findByIsDeleted(isDeleted: Boolean): Comment
}