package cmuguide.demo.entity.dto

class ShopListDto( var id: Long? = null,
                   var name: String? = null,
                   var description: String? = null,
                   var category: String? = null

)