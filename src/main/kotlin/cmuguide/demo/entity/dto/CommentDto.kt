package cmuguide.demo.entity.dto

class CommentDto( var id: Long? = null,
                  var description: String? = null,
                  var isDeleted: Boolean? = false,
                  var userId : Long? = null

)