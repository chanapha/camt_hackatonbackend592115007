package cmuguide.demo.entity

import javax.persistence.*

@Entity
data class ShopList(
        var name: String? = null,
        var description: String? = null,
        var category: String? = null
) {
    @Id
    @GeneratedValue
    var id: Long? = null


    @OneToMany
    var comments = mutableListOf<Comment>()
}