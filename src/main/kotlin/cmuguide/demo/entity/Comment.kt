package cmuguide.demo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class Comment(
         var description: String? = null,
         var isDeleted: Boolean? = false

) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @ManyToOne
    var user: User? = null

}