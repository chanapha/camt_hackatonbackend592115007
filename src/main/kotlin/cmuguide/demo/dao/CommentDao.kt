package cmuguide.demo.dao

import cmuguide.demo.entity.Comment

interface CommentDao{
     fun getComments(): List<Comment>
     fun save(comment: Comment): Comment
     fun getCommentById(commentId: Long): Comment?

}