package cmuguide.demo.dao

import cmuguide.demo.entity.User

interface UserDao{
     fun getUsers(): List<User>
     fun getUsersById(id: Long): User
     fun save(user: User): User


}