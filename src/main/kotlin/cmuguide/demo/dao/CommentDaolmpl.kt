package cmuguide.demo.dao

import cmuguide.demo.entity.Comment
import cmuguide.demo.repository.CommentRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository


@Repository
class CommentDaolmpl: CommentDao {

    @Autowired
    lateinit var commentRepository: CommentRepository

    override fun getCommentById(commentId: Long): Comment? {
        return commentRepository.findById(commentId).orElse(null)
    }

    override fun save(comment: Comment): Comment {
        return commentRepository.save(comment)
    }


    override fun getComments(): List<Comment> {
        return commentRepository.findAll().filterIsInstance(Comment::class.java)
    }
}