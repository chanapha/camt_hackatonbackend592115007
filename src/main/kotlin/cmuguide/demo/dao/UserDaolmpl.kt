package cmuguide.demo.dao

import cmuguide.demo.entity.User
import cmuguide.demo.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Repository
class UserDaolmpl: UserDao {

    @Autowired
    lateinit var userRepository: UserRepository

    override fun save(user: User): User {
        return userRepository.save(user)
    }

    override fun getUsersById(id : Long): User {
        return userRepository.findById(id).orElse(null)

    }

    override fun getUsers(): List<User> {
        return userRepository.findAll().filterIsInstance(User::class.java)
    }

}