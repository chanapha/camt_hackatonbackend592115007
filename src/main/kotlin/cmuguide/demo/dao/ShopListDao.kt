package cmuguide.demo.dao

import cmuguide.demo.entity.Comment
import cmuguide.demo.entity.ShopList

interface ShopListDao{
     fun getComments(): List<ShopList>
     fun getShopListById(shopListId: Long): ShopList
     fun save(shopList: ShopList): ShopList


}