package cmuguide.demo.dao

import cmuguide.demo.entity.ShopList
import cmuguide.demo.repository.ShopListRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository


@Repository
class ShopListDaolmpl: ShopListDao {
    override fun save(shopList: ShopList): ShopList {
        return shopListRepository.save(shopList)
    }

    @Autowired
    lateinit var shopListRepository: ShopListRepository


    override fun getShopListById(shopListId: Long): ShopList {
        return shopListRepository.findById(shopListId).orElse(null)
    }

    override fun getComments(): List<ShopList> {
        return shopListRepository.findAll().filterIsInstance(ShopList::class.java)
    }


}