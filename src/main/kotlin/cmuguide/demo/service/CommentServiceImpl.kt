package cmuguide.demo.service

import cmuguide.demo.dao.CommentDao
import cmuguide.demo.dao.ShopListDao
import cmuguide.demo.dao.UserDao
import cmuguide.demo.entity.Comment
import cmuguide.demo.entity.dto.CommentDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class CommentServiceImpl: CommentService{
    @Autowired
    lateinit var commentDao: CommentDao

    @Autowired
    lateinit var userDao: UserDao

    @Autowired
    lateinit var shopListDao: ShopListDao

    override fun remove(commentId: Long): Comment? {
        val comment = commentDao.getCommentById(commentId)
        comment!!.isDeleted = true
        commentDao.save(comment)
        return comment
    }

    override fun editCommentByCommentId(commentId: Long, description: String): Any {
        var comment = commentDao.getCommentById(commentId)
        comment!!.description = description
        commentDao.save(comment)
        return comment
    }

    @Transactional
    override fun addCommentToShoplist(shopListId: Long, comment: CommentDto): Comment {
        var newUser = userDao.getUsersById(comment.userId!!)
        val newComment = Comment()
        newComment.description = comment.description
        newComment.isDeleted = false
        newComment.user = newUser
        commentDao.save(newComment)
        val shopList = shopListDao.getShopListById(shopListId)
        shopList.comments.add(newComment)
        shopListDao.save(shopList)
        return newComment
    }

    override fun getComments(): List<Comment> {
        return commentDao.getComments().filter { it.isDeleted == false }
    }


}