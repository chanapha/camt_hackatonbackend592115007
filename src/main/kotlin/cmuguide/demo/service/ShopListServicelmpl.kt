package cmuguide.demo.service

import cmuguide.demo.dao.ShopListDao
import cmuguide.demo.entity.Comment
import cmuguide.demo.entity.ShopList
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ShopListServicelmpl: ShopListService{
    override fun getCommentsByShopListId(shopListId: Long): List<Comment> {
        return shopListDao.getShopListById(shopListId).comments
    }


    override fun getShopLists(): List<ShopList> {
        return shopListDao.getComments()

    }

    @Autowired
    lateinit var shopListDao: ShopListDao


}