package cmuguide.demo.service

import cmuguide.demo.entity.User

interface UserService{
    fun getUsers(): List<User>
    fun save(user: User): User

}