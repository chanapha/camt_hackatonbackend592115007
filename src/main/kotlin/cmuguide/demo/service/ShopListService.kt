package cmuguide.demo.service

import cmuguide.demo.entity.Comment
import cmuguide.demo.entity.ShopList

interface ShopListService{
     fun getShopLists(): List<ShopList>
     fun getCommentsByShopListId(shopListId: Long): List<Comment>

}