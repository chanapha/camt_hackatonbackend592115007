package cmuguide.demo.service

import cmuguide.demo.dao.UserDao
import cmuguide.demo.entity.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class UserServicelmpl: UserService{

    @Autowired
    lateinit var userDao: UserDao

    @Transactional
    override fun save(user: User): User {
        return userDao.save(user)
    }

    override fun getUsers(): List<User> {
        return userDao.getUsers()
    }

}