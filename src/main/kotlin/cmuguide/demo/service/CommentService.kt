package cmuguide.demo.service

import cmuguide.demo.entity.Comment
import cmuguide.demo.entity.dto.CommentDto

interface CommentService{
     fun getComments():  List<Comment>
     fun addCommentToShoplist(shopListId: Long, comment: CommentDto): Comment
     fun remove(commentId: Long): Comment?
     fun editCommentByCommentId(commentId: Long, description: String): Any
}