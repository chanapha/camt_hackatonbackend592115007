package cmuguide.demo.util

import cmuguide.demo.entity.Comment
import cmuguide.demo.entity.ShopList
import cmuguide.demo.entity.User
import cmuguide.demo.entity.dto.CommentDto
import cmuguide.demo.entity.dto.ShopListDto
import cmuguide.demo.entity.dto.UserDto
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    fun mapUserDto(user: User?): UserDto?
    fun mapUserDto(user: List<User>): List<UserDto>
    @InheritInverseConfiguration
    fun mapUserDto(userDto: UserDto): User

    fun mapCommentDto(comment: Comment?): CommentDto?
    fun mapCommentDto(comment: List<Comment>): List<CommentDto>
    @InheritInverseConfiguration
    fun mapCommentDto(commentDto: CommentDto): Comment



    fun mapShopListDto(shopList: ShopList?): ShopListDto?
    fun mapShopListDto(shopList: List<ShopList>): List<ShopListDto>
    @InheritInverseConfiguration
    fun mapShopListDto(shopListDto: ShopListDto): ShopList



}